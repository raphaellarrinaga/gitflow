# Gitflow

Testing git merge/rebase

Documentation at <http://www.git-attitude.fr/2014/05/04/bien-utiliser-git-merge-et-rebase/>

- Dans quels cas utiliser merge ou rebase
- Workflow
- true merge et fast-forward
- Cas possibles
  - True merge automatique
  - True merge manuel avec `--no-ff`
  - fast-forward automatique
  - fast-forward avec rebase
- Nettoyer l'historique local
